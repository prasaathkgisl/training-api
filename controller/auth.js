const knex = require("../db/connection");
const helperController = require("../helper/_helper");
const bcrypt = require("bcryptjs");
const Logger = require("../lib/logger").Logger;
const mailController = require("../lib/mail");
const amqp = require("amqplib");
/**
 * sentPinToMail Will Send OTP Through Email And Update OTP Code In Database
 * @param {String} mail
 */
let sentPinToMail = async mail => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : sentPinToMail function}`
    );
    console.log("mail" , mail);
    let response = await knex("users")
      .column("u_email", "u_activated")
      .where({
        u_email: mail
      })
      .first();
    //  console.log("response" , response);
    if (response == undefined) {
      return "email address is not associated with any account";
    }
  
    if (response.u_activated == 0) {
      const connection = await amqp.connect(process.env.QUEUE_URL);
      const channel = await connection.createChannel();
      await channel.assertQueue(process.env.COMMON_QUEUE_NAME);
      let OTP = helperController.generateOtp();
      console.log("OTP" , OTP);
      let sub = "virtualogin - OTP Verification Code";
      let body = "As requested by you, OTP Verification code is " + OTP;
      let email = response.u_email;
      let updateOTPResponse = await updateOTP(email, OTP);
      console.log("updateOTPResponse" , updateOTPResponse);
      // let sendMail = await mailController.sendFormAsMail(sub, body, email);
      const msgBuffer = Buffer.from(JSON.stringify({emailSubject: sub, template: null, type: 'common', emailId: email, htmlData: null, body: body }));
      channel.sendToQueue(process.env.COMMON_QUEUE_NAME, msgBuffer);
      await channel.close();
      await connection.close();
      // if (sendMail.envelope.to.includes(response.u_email)) {
        return "Email Send Successfully";
      // } else {
      //   var e = new Error(`Email Send Failed: ${sendMail}`); // e.name is 'Error'
      //   e = "Email Send Failed";
      //   throw e;
      // }
    } else {
      return "Email Address Already Verified";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : sentPinToMail function, Error:${error}}`
    );
    throw error;
  }
};

/**
 * updateOTP Update OTP Code In DataBase
 * @param {String} email
 * @param {String} otp
 */
let updateOTP = async (email, otp) => {
  try {
    Logger.info(`{ FilePath: ${__filename}  ,Function : updateOTP function}`);
    let updateResponse = await knex("users")
      .where({
        u_email: email
      })
      .update({
        u_new_password_key: otp
      });
    return true;
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : updateOTP function, Error:${error}}`
    );
    throw error;
  }
};

/**
 * verificationEmail will check email the valid
 * @param {object} payload
 */
let verificationEmail = async payload => {
  Logger.info(
    `{ FilePath: ${__filename}  ,Function : verificationEmail started }`
  );
  try {
    let response = await knex("users")
      .column("u_email", "u_activated")
      .where({
        u_email: payload.mail
      })
      .first();
    if (response.activated == false) {
      let verificationEmailResponse = await knex("users")
        .where({
          u_email: payload.mail,
          u_new_password_key: payload.pin
        })
        .update({
          u_activated: 1
        });
      if (verificationEmailResponse) {
        return "Mail Id Verification Successfully";
      } else {
        throw "Mail Id Verification failed";
      }
    } else {
      return "Mail Id Already Verified";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : verificationEmail started, Error: ${error} }`
    );
    throw error;
  }
};

/**
 * changePassword function for change the password
 * @param {Object} payload
 * @param {Number} id
 */
let changePassword = async (payload, id) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : changePassword started }`
    );
    const salt = bcrypt.genSaltSync();
    const hashedPassword = bcrypt.hashSync(payload.new_password, salt);
    let dbPasswordResponse = await knex("users")
      .column("u_password")
      .where({
        u_id: id
      })
      .first();
    let checkOldPassword = await helperController.comparePass(
      payload.old_password,
      dbPasswordResponse.u_password
    );
    if (checkOldPassword) {
      let changePassword = await knex("users")
        .where({
          u_id: id
        })
        .update({
          u_password: hashedPassword
        });
      return "Password changed Successfully";
    } else {
      throw "Old Password MissMatched";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : changePassword failed, Error: ${error} }`
    );
    throw error;
  }
};

module.exports = {
  sentPinToMail,
  verificationEmail,
  changePassword
};
