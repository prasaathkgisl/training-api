const knex = require("../db/connection");
const bcrypt = require('bcrypt');

// export async function createUser(req, res) {
let createUser = async (req, res) => {
    try {
        let checkMail = await knex('users').where({
            u_email: req.body.u_email,
        });
        if (!checkMail.length) {
            // let userdata = {
            //     u_email: req.body.u_email,
            //     u_first_name: req.body.u_first_name,
            //     u_last_name: req.body.u_last_name,
            //     u_password: bcrypt.compare(req.body.u_password, "1234"),
            //     u_last_name: req.body.u_role,
            //     u_last_name: req.body.u_batch_no
            // };
            await knex('users').insert(req.body).returning('id').then( ([resp]) => {       
                res.status(200).send({ status: true, message: 'User created successfully!' });
            });     
        } else {
            throw "Email ID already registered!!"
        }
    } catch (err) {
        res.status(400).send({ status: false, message: err });
    }
}

// export async function updateUser(req, res) {
let updateUser = async (req, res) => {
    try {
        await knex('users').update(req.body).where({
            u_id: req.params.id
        })
        res.status(200).send({ status: true, message: 'User updated successfully.' });   
    } catch (err) {
        res.status(400).send({ status: false, message: err });
    }
}

// export async function deleteUser(req, res) {
let deleteUser = async (req, res) => {
    try {
        await knex('users').update({u_status: false, u_is_deleted: true, u_deleted_on: Date.now()}).where({
            u_id: req.params.id
        })
        res.status(200).send({ status: true, message: "User removed successfully." });    
    } catch (err) {
        res.status(400).send({ status: false, message: err });
    }
}

// export async function findSingleUser(req, res) {
let findSingleUser = async (req, res) => {
    try {
        let user = await knex('users').where({
            u_id: req.params.id,
        });
        res.status(200).send({ status: true, data: user, message: "User fetched successfully." });
    } catch (error) {
        res.status(400).send({ status: false, message: err });
    }        
}

// export async function getAllUsers(req, res) {      
let getAllUsers = async (req, res) => {   
    try {
        let skip = req.query.skip ? req.query.skip : '';
        let limit = req.query.limit ? req.query.limit : '';
        let userFinalList;
        let userList =  knex('users')
        let total_count = await userList.clone().count("u_id as count");
        console.log("total_count:", total_count)
        total_count = total_count[0]["count"];    
        if (skip && limit) {
            userFinalList = await userList.clone().offset(skip).limit(limit);
            userFinalList = await userList.orderBy('u_id', 'desc')
        } else {
            userList = userList.orderBy('u_id', 'desc')
            userFinalList = await userList.clone()
        }
        userFinalList['totalCount'] = total_count
        res.status(200).send({ data: userFinalList, status: true, message: 'Users fetched successfully!' });

    } catch (error) {
        console.log(error)
        res.status(400).send({ status: false, message: error });
    }
}

// export async function findSingleUser(req, res) {
    let getUnsubscriptionReasonList = async (req, res) => {
        try {
            let unsubscription_reason = await knex('unsubscription_reason')
            res.status(200).send({ status: true, data: unsubscription_reason, message: "Unsubscription reason list feched successfully." });
        } catch (error) {
            res.status(400).send({ status: false, message: error });
        }        
    }

    let createUnsubscriptionReason = async (req, res) => {
        try {
            await knex('unsubscription_reason').insert(req.body).returning('id').then( ([resp]) => {       
                res.status(200).send({ status: true, message: 'Unsubscription reason created successfully!' });
            }); 
        } catch (error) {
            res.status(400).send({ status: false, message: error });
        } 
        
    }

    let saveUnsubscription = async (req, res) => {
        try {
            
            const [saveUnsubscription, saveUser] = await Promise.all([
                knex('unsubscription_list').insert(req.body).returning('id'),
                knex('users').update({ u_email_subscription: 0 }).where({ u_id: req.body.un_u_id })
            ])
            res.status(200).send({ status: true, message: 'User unsubscribed successfully!' });
        } catch(error) {
            res.status(400).send({ status: false, message: error });
        }
    }

    let getAllUnsubscriptionList = async (req, res) => {        
        try {           
            let unsubscription_list = await knex('unsubscription_list').leftJoin("users", "users.u_id", "=", "unsubscription_list.un_u_id").where({'users.u_email_subscription' : 0 });           
            let total_count = await knex.from('unsubscription_list')
            .leftJoin("users", "users.u_id", "=", "unsubscription_list.un_u_id")
            .count('u_id', {as: 'count'})
            .where({'users.u_email_subscription' : 0 });  
           return { data : unsubscription_list, count : total_count[0].count };
        } catch (error) {
            return error;
        }
    }

    let getAllSMSUnsubscriptionList = async (req, res) => {
        try {           
            let unsubscription_list = await knex('unsubscription_list').leftJoin("users", "users.u_id", "=", "unsubscription_list.un_u_id").where({'users.u_sms_subscription' : 0 });           
            let total_count = await knex.from('unsubscription_list')
            .leftJoin("users", "users.u_id", "=", "unsubscription_list.un_u_id")
            .count('u_id', {as: 'count'})
            .where({'users.u_sms_subscription' : 0 });  
           return { data : unsubscription_list, count : total_count[0].count };
        } catch (error) {
            return error;
        }
    }

module.exports = {
    createUser,
    updateUser,
    deleteUser,
    findSingleUser,
    getAllUsers,
    getUnsubscriptionReasonList,
    createUnsubscriptionReason,
    saveUnsubscription,
    getAllUnsubscriptionList,
    getAllSMSUnsubscriptionList
};