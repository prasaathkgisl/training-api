const knex = require("../db/connection");
const Logger = require("../lib/logger").Logger;
const bcrypt = require("bcryptjs");
const helperController = require("../helper/_helper");
const mailController = require("../lib/mail");
const amqp = require("amqplib");
/**
 * checkUserAndSendPin function for check account exist and sid pin to mail
 * @param {String} mail
 */
let checkUserAndSendPin = async (mail) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : checkUserAndSendPin function}`
    );
    // const connection = await amqp.connect(process.env.QUEUE_URL);
    // const channel = await connection.createChannel();
    // await channel.assertQueue(process.env.COMMON_QUEUE_NAME);
    let checkUser = await helperController.checkEmailIdExist(mail);
    // console.log("checkUser::", checkUser)
    if (checkUser == true) {
      let getUser = await knex("users")
        .column("u_id", "u_first_name", "u_last_name", "u_email")
        .where({
          u_email: mail,
        })
        .first();
      let pin = await helperController.generateOtp();
      let sub = "KG invicta Survey - Forget Password PIN";
      let body =
        "As requested by you, Forget Password PIN is " + "<b>" + pin + "</b>";
      let email = getUser.u_email;
      let updatePINResponse = await updatePIN(email, pin);
      let sendMail = await mailController.sendFormAsMail(sub, body, email);
      const msgBuffer = Buffer.from(JSON.stringify({emailSubject: sub, template: null, type: 'common', emailId: email, htmlData: null, body: body }));
      console.log("msgBuffer::",msgBuffer)
      // channel.sendToQueue(process.env.COMMON_QUEUE_NAME, msgBuffer);
      // await channel.close();
      // await connection.close();
      if (sendMail.envelope.to.includes(getUser.u_email)) {
        return "Email Send Successfully";
      } else {
        throw "Email Send Failed";
      }
    } else {
      throw "Account Miss Match";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : checkUserAndSendPin function, Error:${error}}`
    );
    console.log(error)
    throw error;
  }
};

/**
 * updatePIN function for change the generated pin in db
 * @param {String} email
 * @param {Number} pin
 */
let updatePIN = async (email, pin) => {
  let updateResponse = await knex("users")
    .where({
      u_email: email,
    })
    .update({
      u_new_password_key: pin,
      u_new_password_requested: knex.fn.now(),
    });
  return true;
};

/**
 * checkPINAndChangePassword function will check the pin and change the password
 * @param {Object} payload
 */
let checkPINAndChangePassword = async (payload) => {
  try {
    //console.log(payload);
    const salt = bcrypt.genSaltSync();
    const hash = await bcrypt.hashSync(payload.password, salt);

    Logger.info(
      `{ FilePath: ${__filename}  ,Function : checkPINAndChangePassword started}`
    );
    //  console.log("start" , "checkPINAndChangePassword")
    let response = await knex("users")
      .column("u_email")
      .where({
        u_email: payload.email,
        u_new_password_key: payload.pin,
      })
      .first();
    //console.log("response" , "checkPINAndChangePassword")
    if (response !== undefined) {
      let updatePassword = await knex("users")
        .where({
          u_email: response.u_email,
        })
        .update({
          u_password: hash,
        });
      return true;
    } else {
      return false;
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : checkPINAndChangePassword , Error: ${error} }`
    );
    return error;
  }
};

module.exports = {
  checkUserAndSendPin,
  checkPINAndChangePassword,
};
