const bcrypt = require("bcryptjs");
const knex = require("../db/connection");
const Logger = require("../lib/logger").Logger;
const tokenController = require("../lib/token");
const _helper = require("../helper/_helper");
const email_helper = require("../helper/email_helper");
const randomstring = require("randomstring");
const moment = require("moment");
let XLSX = require("xlsx");
let ejs = require("ejs");
let fs = require("fs");
const amqp = require("amqplib");
let statusLogin = { success: "success", failed: "failed" };
// get Users
let getUsers = async (payload) => {
  try {
    let where = {
      // u_activated: 1,
      u_status: 1,
    };
    if (payload.id) {
      where.u_id = payload.id;
    }
    let whereRaw = "";
    if (payload.search) {
      whereRaw =
        "u_last_name like '%" +
        payload.search +
        "%' or u_last_name like '%" +
        payload.search +
        "%' or u_email like '%" +
        payload.search +
        "%'";
    }
    let Users;
    let getUser = knex("users").where(where);
    if (whereRaw != "") {
      getUser.whereRaw(whereRaw);
    }
    if (payload.batch) {
      // getUser = getUser.where('u_batch_no', '=', payload.batch);
      getUser.whereRaw("u_batch_no = " + payload.batch);
    }
    if (payload.role) {
      getUser.whereRaw('u_role = "' + payload.role + '"');
    }
    if (payload.fromDate && payload.toDate) {
      getUser.whereRaw(
        '(DATE(u_created) BETWEEN "' +
          payload.fromDate +
          '" AND "' +
          payload.toDate +
          '")'
      );
    }
    let total_count = await getUser.clone().count("u_id as count");
    total_count = total_count[0]["count"];

    var sortColumn = "u_first_name";
    var sortDir = "asc";
    if (payload.sortColumn) {
      sortColumn = payload.sortColumn;
    }
    if (payload.sortDir) {
      sortDir = payload.sortDir;
    }
    Users = await getUser.orderBy(sortColumn, sortDir);
    if (payload.limit && payload.skip) {
      // getUser.offset(payload.skip).limit(payload.limit)
      Users = await getUser.clone().offset(payload.skip).limit(payload.limit);
    } else {
      Users = await getUser.clone();
    }
    for (const item of Users) {
      item.u_dob = moment(item.u_dob).format("YYYY-MM-DD");
      item.u_created = moment(item.u_created).format("YYYY-MM-DD");
      // console.log("item.u_dob", item.u_dob);
    }
    return {
      data: Users,
      count: total_count,
    };
  } catch (error) {
    console.log(error);
    throw error;
  }
};

let bulkUploadUsers = async (payload, getUserId) => {
  try {
    // console.log("payload::", payload.files)
    const alternativeHeaderExcel = [
      "sno",
      "employeeId",
      "firstName",
      "lastName",
      "email",
      "dob",
      "role",
      "phoneNumber",
      "batchNumber",
    ];
    const workSheetsFromFile = XLSX.readFile(`${payload.files.file.path}`, {
      cellDates: true,
    });
    let firstSheet = workSheetsFromFile.SheetNames[0];
    let excelRows = XLSX.utils.sheet_to_row_object_array(
      workSheetsFromFile.Sheets[firstSheet],
      {
        header: alternativeHeaderExcel,
        skipHeader: true,
      }
    );
    // excelRows = excelRows.slice(1);
    excelRows.shift();
    let successCount = 0;
    let failedCount = 0;
    for await (const x of excelRows) {
      console.log("Innnn");
      let user = await _helper.checkEmailIdExist(x.email);
      console.log("user::", user);
      if (user) {
        x.logStatus = false;
        x.errorMessage = "User email already found";
        failedCount++;
        // continue;
      }
      let phone = await _helper.validatePhoneNumber(x.phoneNumber);
      console.log("phone::", x.phoneNumber, phone);
      if (!phone) {
        x.logStatus = false;
        x.errorMessage = "Phone not valid";
        failedCount++;
        // continue;
      }

      let email = await _helper.validateEmailNew(x.email);
      if (!email) {
        x.logStatus = false;
        x.errorMessage = "EMail not valid";
        failedCount++;
        // continue;
      }

      let dob = await _helper.validateDOB(x.dob);
      if (!dob) {
        x.logStatus = false;
        x.errorMessage = "Invalid DOB format";
        failedCount++;
      }

      if (!user && phone && email && dob) {
        await createUser({
          u_first_name: x.firstName,
          u_last_name: x.lastName,
          u_email: x.email,
          u_role: x.role,
          u_dob: x.dob,
          u_phone: x.phoneNumber,
          u_batch_no: x.batchNumber,
          u_emp_id: x.employeeId,
        });
        x.logStatus = true;
        x.errorMessage = "success";
        successCount++;
      }
    }
    const tmpl = fs.readFileSync(
      require.resolve(`./template/bulk-user-create.ejs`),
      "utf8"
    );
    // const html = ejs.render(tmpl, {
    //   // user: userDetails,
    //   // logo: process.env.CLIENT_LOGO,
    //   // reply_email: emailCredentials.reply_email,
    //   list: excelRows,
    //   userCreated: successCount,
    //   userCreatedFailed: failedCount,
    // });
    const connection = await amqp.connect(process.env.QUEUE_URL);
    const channel = await connection.createChannel();
    await channel.assertQueue(process.env.COMMON_QUEUE_NAME);
    let getUser = await knex("users").where({ u_id: getUserId }).first();
    // let email = await email_helper.sendWelcomeEmail(
    //   "Bulk Import New Users",
    //   html,
    //   getUser.u_email
    // );
    const msgBuffer = Buffer.from(
      JSON.stringify({
        emailSubject: "Bulk Import New Users",
        template: "bulk-user-create",
        type: "common",
        emailId: getUser.u_email,
        htmlData: {
          list: excelRows,
          userCreated: successCount,
          userCreatedFailed: failedCount,
        },
      })
    );
    channel.sendToQueue(process.env.COMMON_QUEUE_NAME, msgBuffer);
    await channel.close();
    await connection.close();
    return { msg: "User uploaded successfully", data: "" };
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * createUser function for added new user
 * @param {Object} payload
 */
let createUser = async (payload) => {
  try {
    Logger.info(`{ FilePath: ${__filename}  ,Function : createUser function}`);
    let checkemail = await _helper.checkEmailIdExist(payload.u_email);
    if (checkemail == true) {
      throw "email already exist!!";
    }
    let u_password = randomstring.generate({
      length: 8,
      charset: "numeric",
    });

    let salt = bcrypt.genSaltSync();
    var hash = bcrypt.hashSync(u_password, salt);

    let insertData = {
      u_first_name: payload.u_first_name,
      u_last_name: payload.u_last_name,
      u_email: payload.u_email,
      u_password: hash,
      u_role: payload.u_role,
      u_activated: 1,
      u_activate_otp: 0,
    };
    if (payload.u_dob) {
      insertData.u_dob = payload.u_dob;
    }
    if (payload.u_phone) {
      insertData.u_phone = payload.u_phone;
    }
    let response = await knex("users").insert(insertData).returning("u_id");
    let insertDataID = response[0];
    // insertData = response[0];
    let subject = "Training";
    console.log(subject, "Subject");
    console.log(insertData.u_email, "To Address");
    await email_helper.sendWelcomeEmail(
      subject,
      "your account is created and your password is:" + u_password,
      insertData.u_email
    );
    return { userId: insertDataID };
  } catch (error) {
    console.log(error);
    Logger.error(
      `{FilePath: ${__filename}  ,Function : createUser function, Error:${error}}`
    );
    throw error;
  }
};
/**
 * @param {String} email
 * @param {String} password
 */
let loginAdmin = async (email, password) => {
  try {
    Logger.info(`{ FilePath: ${__filename}  ,Function : loginUser function}`);
    let user = await getUser(email);
    if (user == undefined) {
      throw "Invalid Username or Password";
    } else if (user.u_activated == 0) {
      throw "Your account is deactivated";
    } else if (user.u_status == 0) {
      throw "Your account is disabled";
    } else if (user.u_banned == 0) {
      throw "Your are banned";
    } else if (user.u_is_admin == false) {
      throw "Your are not admin";
    }
    let passwordMatched = await _helper.comparePass(password, user.u_password);
    if (passwordMatched) {
      let token = await tokenController.token(user);
      return {
        token: token["token"],
        user: user,
        cartcount: token["cartcount"],
        favcount: token["favcount"],
      };
    } else {
      throw "Invalid Username or Password";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : loginUser function, Error:${error}}`
    );
    throw error;
  }
};
/**
 * @param {String} email
 * @param {String} password
 */
let loginUser = async (email, password, payload) => {
  try {
    Logger.info(`{ FilePath: ${__filename}  ,Function : loginUser function}`);
    console.log(email, "L email");
    console.log(password, "L pass");

    let user = await getUser(email);
    if (user == undefined) {
      loginLog({ l_user_name: email, l_login_status: statusLogin.failed });
      throw "Invalid Username or Password";
    } else if (user.u_activated == 0) {
      loginLog({ l_user_name: email, l_login_status: statusLogin.failed });
      throw "Your account is deactivated";
    } else if (user.u_status == 0) {
      loginLog({ l_user_name: email, l_login_status: statusLogin.failed });
      throw "Your account is disabled";
    } else if (user.u_banned == 0) {
      loginLog({ l_user_name: email, l_login_status: statusLogin.failed });
      throw "Your are banned";
    }
    let passwordMatched = await _helper.comparePass(password, user.u_password);
    if (passwordMatched) {
      let data = {
        u_last_login: knex.fn.now(),
        u_last_ip: payload.connection.remoteAddress,
      };
      let updateLogin = await knex("users")
        .where({
          u_id: user.u_id,
        })
        .update(data);
      let token = await tokenController.token(user);
      loginLog({ l_user_name: email, l_login_status: statusLogin.success });
      return {
        token: token["token"],
        user: user,
      };
    } else {
      loginLog({ l_user_name: email, l_login_status: statusLogin.failed });
      throw "Invalid Username or Password";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : loginUser function, Error:${error}}`
    );
    throw error;
  }
};
let verifytoken = async (payload) => {
  try {
    let checktoken = await tokenController.jwtVerify(payload.token);

    console.log(checktoken);
    let user = await knex("users")
      .where({
        u_id: checktoken.userId,
      })
      .first();
    let user1 = await getUser(user.u_email);
    let token = await tokenController.token(user1);
    let checkTimeValue = await checkTimeIsValid(checktoken.surveyListId);
    // console.log("checkTimeValue::", checkTimeValue)
    return {
      token: token["token"],
      user: user1,
      surveyListId: checktoken.surveyListId,
      accessTimeValid: checkTimeValue,
    };
  } catch (error) {
    console.log(error);
    return {
      status: false,
      error: error,
    };
    // console.log(error);
  }
};

let checkTimeIsValid = async (surveyListId) => {
  try {
    // console.log("surveyListId::", surveyListId)
    let surveyList = await knex("survey_list")
      .where({
        sl_id: surveyListId,
      })
      .first();
    // console.log("surveyList::", surveyList)
    if (surveyList) {
      // console.log("Inn")
      if (surveyList.sl_from_date && surveyList.sl_to_date) {
        let currentDate = moment();
        let fromDateFormat = moment(
          surveyList.sl_from_date,
          "YYYY-MM-DD  HH:mm:ss"
        );
        let isafter = moment(currentDate).isAfter(surveyList.sl_from_date);
        let toDateFormat = moment(
          surveyList.sl_to_date,
          "YYYY-MM-DD  HH:mm:ss"
        );
        let isBefore = moment(currentDate).isBefore(surveyList.sl_to_date);
        console.log(
          "isafter::",
          surveyListId,
          isafter,
          isBefore,
          currentDate,
          fromDateFormat,
          toDateFormat
        );
        // if(isafter && isBefore) return true;
        // else return false
        let notStarted = isafter ? false : true;
        let expired = isBefore ? false : true;
        return {
          notStarted: notStarted,
          expired: expired,
          fromDate: surveyList.sl_from_date,
          endDate: surveyList.sl_to_date,
        };
      } else {
        return {
          notStarted: false,
          expired: false,
          fromDate: null,
          endDate: null,
        };
      }
    } else {
      return {
        notStarted: false,
        expired: false,
        fromDate: null,
        endDate: null,
      };
    }

    // return
  } catch (error) {
    console.log(error);
    throw error;
    // console.log(error);
  }
};

/**
 * getUser function to get the user detail by checking Email ID
 * @param {String} payload
 */
let getUser = async (payload) => {
  try {
    Logger.info(`{ FilePath: ${__filename}  ,Function : getUser function}`);
    let checkEmailOrPhone = await _helper.validateEmail(payload);
    if (checkEmailOrPhone === true) {
      let insertData = {
        "users.u_email": payload,
      };
      let data = await knex("users").where(insertData).first();
      return data;
    } else {
      throw "email not valid!!";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : getUser function, Error:${error}}`
    );
    throw error;
  }
};
let saveProfile = async (data, user_id) => {
  try {
    data.u_modified = knex.fn.now();
    let updatePassword = await knex("users")
      .where({
        u_id: user_id,
      })
      .update(data);
    return updatePassword;
  } catch (error) {
    throw error;
  }
};

let updateUser = async (payload, user_id, req) => {
  try {
    let updateData = {
      u_first_name: payload.u_first_name,
      u_last_name: payload.u_last_name,
      u_dob: payload.u_dob,
      u_batch_no: payload.u_batch_no,
      u_phone: payload.u_phone,
      u_activated: payload.u_activated,
      u_emp_id: payload.u_emp_id,
      u_role: payload.u_role
    };
    updateData.u_modified = knex.fn.now();
    let updatePassword = await knex("users")
      .where({
        u_id: user_id,
      })
      .update(updateData);
    return updatePassword;
  } catch (error) {
    throw error;
  }
};

let getuserdata = async (data) => {
  try {
    console.log(data);
    let getdata = await knex("users").where({
      u_id: data.u_id,
    });
    var senddata = getdata[0];
    var datas = {
      firstname: senddata.u_first_name,
      lastname: senddata.u_last_name,
      username: senddata.u_username,
      phone: senddata.u_phone,
      email: senddata.u_email,
      emp_code: senddata.u_employee_code,
      emp_role: senddata.u_employee_role,
      dob: moment(senddata.u_dob).format("YYYY-MM-DD"),
      profile_img: senddata.profile_img,
      telegram_chat_id: senddata.u_telegram_chat_id,
    };
    return datas;
  } catch (error) {
    throw error;
  }
};

let gettrainerdata = async (data) => {
  try {
    let getdata = await knex("users").where({
      u_role: "trainer",
    });
    // console.log(getdata);
    return getdata;
  } catch (error) {
    throw error;
  }
};

let getRoleData = async (userId) => {
  try {
    let getdata = await knex("users")
      .select("u_role")
      .where({
        u_id: userId,
      })
      .first();
    return getdata;
  } catch (error) {
    throw error;
  }
};

let updateSubscription = async (data) => {
  try {
    let getdata = await knex("users")
      .update({
        u_subscription: 0,
      })
      .where({
        u_id: data.id,
      });
    // console.log(getdata);
    return getdata;
  } catch (error) {
    throw error;
  }
};

let loginLog = async (data) => {
  try {
    let response = await knex("login_log").insert(data).returning("l_id");
    return response;
  } catch (error) {
    throw error;
  }
};

let loginLogList = async (payload) => {
  try {
    let where = {
      // u_activated: 1,
      l_status: 1,
    };

    let Users;
    let getUser = knex("login_log").where(where);

    let total_count = await getUser.clone().count("l_id as count");
    total_count = total_count[0]["count"];

    var sortColumn = "l_created";
    var sortDir = "asc";
    if (payload.sortColumn) {
      sortColumn = payload.sortColumn;
    }
    if (payload.sortDir) {
      sortDir = payload.sortDir;
    }
    Users = await getUser.orderBy(sortColumn, sortDir);
    if (payload.limit && payload.skip && !payload.batch && !payload.role) {
      Users = await getUser.clone().offset(payload.skip).limit(payload.limit);
    } else {
      Users = await getUser.clone();
    }
    for (const item of Users) {
      item.l_created = moment(item.l_created).format("YYYY-MM-DD HH:mm");
    }
    return {
      data: Users,
      count: total_count,
    };
  } catch (error) {
    console.log(error);
    throw error;
  }
};

let userDashboard = async (payload) => {
  try {
    let totalUser = await knex("users").count("users.u_id AS count");
    let activeUser = await knex("users")
      .count("users.u_id AS count")
      .where({ u_status: 1, u_activated: 1 });
    let disabledUser = await knex("users")
      .count("users.u_id AS count")
      .where({ u_activated: 0, u_status: 1 });
    let deletedUser = await knex("users")
      .count("users.u_id AS count")
      .where({ u_status: 0 });
    let successLoginCount = await knex("login_log")
      .count("login_log.l_id AS count")
      .where({ l_login_status: statusLogin.success });
    let failedLoginCount = await knex("login_log")
      .count("login_log.l_id AS count")
      .where({ l_login_status: statusLogin.failed });
    return {
      totalUser: totalUser.length > 0 ? totalUser[0]["count"] : 0,
      activeUser: activeUser.length > 0 ? activeUser[0]["count"] : 0,
      disabledUser: disabledUser.length > 0 ? disabledUser[0]["count"] : 0,
      deletedUser: deletedUser.length > 0 ? deletedUser[0]["count"] : 0,
      successLoginCount:
        successLoginCount.length > 0 ? successLoginCount[0]["count"] : 0,
      failedLoginCount:
        failedLoginCount.length > 0 ? failedLoginCount[0]["count"] : 0,
    };
  } catch (error) {
    console.log(error);
    throw error;
  }
};

let getCountry = async (data) => {
  try {
    let getdata = await knex("country_city")
      .select("c_country")
      .where({
        l_status: 1,
      })
      .groupBy("c_country");
    // console.log(getdata);
    return getdata;
  } catch (error) {
    throw error;
  }
};

let getCity = async (data) => {
  try {
    let getdata = await knex("country_city").select("*").where({
      l_status: 1,
      c_country: data,
    });
    // console.log(getdata);
    return getdata;
  } catch (error) {
    throw error;
  }
};

let getCountryCountDetails = async (data) => {
  try {
    let getdata = await knex("country_city").select("*").where({
      l_status: 1,
      c_country: data,
    });

    let cityCount = getdata.length;

    let totalPopulation = 0;
    for (let i in getdata) {
      totalPopulation += parseInt(getdata[i]["c_population"]);
    }
    return { total_cities: cityCount, population: totalPopulation };
  } catch (error) {
    throw error;
  }
};

module.exports = {
  createUser,
  loginUser,
  saveProfile,
  loginAdmin,
  getUsers,
  updateUser,
  getuserdata,
  gettrainerdata,
  bulkUploadUsers,
  verifytoken,
  checkTimeIsValid,
  updateSubscription,
  getRoleData,
  loginLogList,
  userDashboard,
  getCountry,
  getCity,
  getCountryCountDetails,
};
