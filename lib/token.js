const moment = require("moment");
const randToken = require("rand-token");
const JWT = require("jsonwebtoken");
// const mongoose = require("mongoose");
const knex = require("../db/connection");
const Logger = require("../lib/logger").Logger;
const jwt = require("jwt-simple");
// mongoose.set("useNewUrlParser", true);
// mongoose.set("useFindAndModify", false);
// mongoose.set("useCreateIndex", true);
// mongoose.connect("mongodb://localhost:27017/myapp", {
//   useUnifiedTopology: true,
// });

// let tokensSchema = new mongoose.Schema({
//   user_id: {
//     type: Number,
//     unique: true,
//   },
//   refreshToken: String,
//   created: {
//     type: Date,
//     default: Date.now,
//   },
// });

// let Tokens = mongoose.model("Tokens", tokensSchema);

/**
 * getNewRefreshToken function to generate new Refresh Token
 * @param {Object} payload
 */
let getNewRefreshToken = async (payload) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : getNewRefreshToken started }`
    );
    let data = await Tokens.find({
      user_id: payload.user_id,
    });
    if (
      payload.refreshToken == data[0].refreshToken &&
      payload.user_id == data[0].user_id
    ) {
      let find = {
        u_id: payload.user_id,
      };
      let userDetail = await findUse(find);
      let tokenRespons = await token(userDetail);
      return tokenRespons;
    } else {
      return "error";
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : getNewRefreshToken failed, Error: ${error} }`
    );
    throw error;
  }
};

/**
 * token Function to generate a Access and Refresh Token
 * @param {Object} user
 */
let token = async (user) => {
  try {
    Logger.info(`{ FilePath: ${__filename}  ,Function : token started }`);
    let find = {
      u_id: user.u_id,
    };
    let userdata = await findUse(find);
    let sub = userdata.u_id;
    let is_admin = userdata.u_is_admin;
    let admin_type = userdata.u_admin_type;
    let status = userdata.u_status;

    let refreshTokens = {};
    var token = JWT.sign(
      {
        sub,
        is_admin,
        admin_type,
        status,
      },
      process.env.TOKEN_SECRET,
      {
        expiresIn: 36000000,
      }
    );
    var refreshToken = randToken.uid(256);
    refreshTokens["token"] = {
      accessToken: token,
      // refreshToken: refreshToken,
    };
    refreshTokens["user"] = userdata;
    // let insertResponse = await insertAndUpdateRefreshTokenInDb(
    //   sub.user_id,
    //   refreshToken
    // );
    return refreshTokens;
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : token failed, Error: ${error} }`
    );
    throw error;
  }
};

/**
 * insertAndUpdateRefreshTokenInDb function to insert and update the refresh token in mongoDB
 * @param {Number} userId
 * @param {String} refreshToken
 */
let insertAndUpdateRefreshTokenInDb = async (userId, refreshToken) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : insertAndUpdateRefreshTokenInDb started }`
    );
    let conditions = {
      user_id: userId,
    };
    let update = {
      refreshToken: refreshToken,
    };
    let getUserId = await Tokens.find({
      user_id: userId,
    });
    if (getUserId.length > 0) {
      updateUserId = await Tokens.updateOne(conditions, update);
    } else {
      createUserId = await Tokens.create({
        user_id: userId,
        refreshToken: refreshToken,
      });
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : insertAndUpdateRefreshTokenInDb failed, Error: ${error} }`
    );
    throw error;
  }
};

/**
 * findUse function to find the user details
 * @param {object} payload
 */
let findUse = async (payload) => {
  try {
    Logger.info(`{ FilePath: ${__filename}  ,Function : findUse started }`);
    let userDetails = await knex("users").where(payload).first();
    return userDetails;
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : findUse failed, Error: ${error} }`
    );
    throw error;
  }
};

function decodeToken(token, callback) {
  const payload = jwt.decode(token, process.env.TOKEN_SECRET, true, "HS512");
  const now = moment().unix();
  // check if the token has expired
  if (now > payload.exp) callback("Token has expired.");
  else callback(null, payload);
}

function jwtSign(payload) {
    let token = JWT.sign(payload, process.env.TOKEN_SECRET,{expiresIn: 5184000000});
    return token
}

function jwtVerify(token) {
  let jwtVerifyToken = JWT.verify(token, process.env.TOKEN_SECRET);
  return jwtVerifyToken
}

module.exports = {
  token,
  getNewRefreshToken,
  findUse,
  decodeToken,
  jwtSign,
  jwtVerify
};
