const nodemailer = require("nodemailer");
const Logger = require("../lib/logger").Logger;
const util = require("util");
/**
 * To send mail to user
 * @param {String} sub
 * @param {String} body / can be html
 * @param {String} toAddr / is a email address
 */
let sendFormAsMail = async (sub, body, toAddr) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : sendFormAsMail started}`
    );
    let smtpTransport = nodemailer.createTransport({
      service: "Gmail",
      auth: {
        user: "kgtraining2023@gmail.com",
        pass: "qnajtjzkesvfbdue",
      },
    });

    let mailOptions = {
      from: "Training <no-reply@kginvicta.com>",
      to: toAddr,
      subject: sub != undefined ? sub : "Training",
      html: body,
    };
    let sendEmail = await smtpTransport.sendMail(mailOptions);
    console.log("sendEmail", sendEmail);
    return sendEmail;
  } catch (error) {
    console.log("sendEmail", error);
    throw error;
  }
};

module.exports.sendFormAsMail = sendFormAsMail;
