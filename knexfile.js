require("dotenv").config();
const config = {
  client: "mysql2",
  connection: {
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
  debug: ["development", "test"].includes(process.env.NODE_ENV),
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    tableName: "_migrations",
    loadExtensions: [".js"],
    directory: "./database_config/migrations",
  },
  seeds: {
    directory: "./database_config/seeds",
    loadExtensions: [".js"],
  },
};

module.exports = {
  development: config,
  test: config,
  staging: config,
  production: config,
};
