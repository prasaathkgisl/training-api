var express = require('express');
var router = express.Router();
const passport = require("passport");

let userController = require('../controller/user.controller')

router.get('/', userController.getAllUsers);
router.post('/', userController.createUser);
router.put('/:id', userController.updateUser);
router.delete('/:id', userController.deleteUser);
router.get('/:id', userController.findSingleUser);
router.get('/unsubscription/reason_list', passport.authorize("jwt"), userController.getUnsubscriptionReasonList);
router.post('/unsubscription_reason', passport.authorize("jwt"), userController.createUnsubscriptionReason);
router.post('/unsubscription/save', passport.authorize("jwt"), userController.saveUnsubscription);
// router.get('/unsubscription/getAll', passport.authorize("jwt"), userController.getAllUnsubscriptionList);

router.get("/unsubscription/getAll", passport.authorize("jwt"), async (req, res, next) => {
    try {
      var data = await userController.getAllUnsubscriptionList();
      responseFormat(res, 200, "success", data);
    } catch (error) {
      console.log(error)
      responseFormat(res, 422, "failed", error);
    }
});

router.get("/unsubscription/getAllSMS", passport.authorize("jwt"), async (req, res, next) => {
    try {
      var data = await userController.getAllSMSUnsubscriptionList();
      responseFormat(res, 200, "success", data);
    } catch (error) {
      console.log(error)
      responseFormat(res, 422, "failed", error);
    }
});

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

module.exports = router;
