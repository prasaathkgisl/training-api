var express = require("express");
var router = express.Router();
const { check, oneOf, validationResult } = require("express-validator");
const userController = require("../controller/user_management");
const tokenController = require("../lib/token");
const helperController = require("../helper/_helper");
const passport = require("passport");
const multer = require("multer");
const multerStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public");
  },
  filename: (req, file, cb) => {
    const ext = file.mimetype.split("/")[1];
    cb(null, `profileimage/user-${file.fieldname}-${Date.now()}.${ext}`);
  },
});
const upload = multer({
  storage: multerStorage,
});
var multipart = require("connect-multiparty");
var multipartMiddleware = multipart();

router.get("/", passport.authorize("jwt"), async (req, res, next) => {
  try {
    var data = await userController.getUsers(req.query);
    responseFormat(res, 200, "success", data);
  } catch (error) {
    responseFormat(res, 422, "failed", error);
  }
});

router.post(
  "/bulk-upload",
  multipartMiddleware,
  passport.authorize("jwt"),
  async (req, res, next) => {
    try {
      let getUserId = await helperController.parseTokenToGetId(
        req.headers.authorization
      );
      var data = await userController.bulkUploadUsers(req, getUserId);
      responseFormat(res, 200, "success", data);
    } catch (error) {
      console.log(error);
      responseFormat(res, 422, "failed", error);
    }
  }
);

router.post(
  "/signUp",
  [
    check("u_first_name", "firstname is required")
      .isLength({
        min: 1,
      })
      .exists(),
    check("u_last_name", "lastname is required")
      .isLength({
        min: 1,
      })
      .exists(),
    check("u_email", "email is required").isEmail().isLength({
      min: 1,
    }),
    passport.authorize("jwt"),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      let creatUserResult = await userController.createUser(req.body);
      // let getToken = await tokenController.token({
      //   u_id: creatUserResult.userId,
      // });
      // if (getToken) {
      //   await helperController.putLoginLog(creatUserResult.userId, req);
        responseFormat(res, 200, "success", creatUserResult);
      // } else {
      //   throw getToken;
      // }
    } catch (error) {
      console.log(error);

      helperController.putlog("error", req, {
        FilePath: __filename,
        Function: "signUp error",
        error: error,
        req_data: req.query,
      });
      if (error.constraint == null && error.message == undefined) {
        responseFormat(res, 409, "failed", {
          error: error,
        });
      } else if (error.message != undefined) {
        responseFormat(res, 409, "failed", {
          error: error.mapped(),
        });
      } else {
        let errName = error.constraint.split("_");
        responseFormat(res, 422, "failed", {
          error: `${errName[2]} already exists`,
        });
      }
    }
  }
);
router.post(
  "/login",
  [
    check("email", "email is required")
      .isEmail()
      .isLength({
        min: 1,
      })
      .exists(),
    check("password", "password is required")
      .isLength({
        min: 1,
      })
      .exists(),
  ],
  async (req, res, next) => {
    try {
      //  console.log("req" ,req.body);
      validationResult(req).throw();
      const email = req.body.email;
      const password = req.body.password;
      let responseData = await userController.loginUser(email, password, req);
      if (responseData) {
        await helperController.putLoginLog(responseData.user.u_id, req);
        responseFormat(res, 200, "success", responseData);
      } else {
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      helperController.putlog("error", req, {
        FilePath: __filename,
        Function: "signin error",
        error: error,
        req_data: req.query,
      });
      responseFormat(res, 409, "failed", error);
    }
  }
);
router.post(
  "/loginAdmin",
  [
    check("email", "email is required")
      .isEmail()
      .isLength({
        min: 1,
      })
      .exists(),
    check("password", "password is required")
      .isLength({
        min: 1,
      })
      .exists(),
  ],
  async (req, res, next) => {
    try {
      validationResult(req).throw();
      const email = req.body.email;
      const password = req.body.password;
      let responseData = await userController.loginAdmin(email, password);
      if (responseData) {
        await helperController.putLoginLog(responseData.user.u_id, req);
        responseFormat(res, 200, "success", responseData);
      } else {
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      helperController.putlog("error", req, {
        FilePath: __filename,
        Function: "signin error",
        error: error,
        req_data: req.query,
      });
      responseFormat(res, 409, "failed", error);
    }
  }
);
router.post("/refreshToken", async (req, res, next) => {
  try {
    let responseData = await tokenController.getNewRefreshToken(req.body);
    if (responseData !== "error") {
      responseFormat(res, 200, "success", responseData);
    } else {
      responseFormat(res, 409, "failed", responseData);
    }
  } catch (error) {
    responseFormat(res, 409, "failed", error);
  }
});

router.post("/checkToken", async (req, res, next) => {
  try {
    let responseData = await userController.verifytoken(req.body);
    if (responseData !== "error") {
      responseFormat(res, 200, "success", responseData);
    } else {
      responseFormat(res, 409, "failed", responseData);
    }
  } catch (error) {
    responseFormat(res, 409, "failed", error);
  }
});

router.put(
  "/updateUser",
  // [
  //   check("u_firstname", "firstname is required")
  //     .isLength({
  //       min: 1,
  //     })
  //     .exists(),
  //   check("u_lastname", "lastname is required")
  //     .isLength({
  //       min: 1,
  //     })
  //     .exists(),
  //   check("u_email", "email is required")
  //     .isEmail()
  //     .isLength({
  //       min: 1,
  //     })
  //     .exists(),
  //   check("u_dob", "dob is required")
  //     .isLength({
  //       min: 1,
  //     })
  //     .exists(),
  //   check("u_phone", "phone is required")
  //     .isLength({
  //       min: 10,
  //     })
  //     .exists(),

  //   check("u_username", "username is required")
  //     .isLength({
  //       min: 4,
  //     })
  //     .exists(),
  //   check("u_employee_code", "employee_code is required")
  //     .isLength({
  //       min: 1,
  //     })
  //     .exists(),
  //   check("u_employee_role", "employee_role is required")
  //     .isLength({
  //       min: 1,
  //     })
  //     .exists(),
  //   // check("u_role_id", "role_id is required")
  //     // .isLength({
  //       // min: 1,
  //     // })
  //     // .exists(),
  // ],
  passport.authorize("jwt"),
  upload.single("image"),
  async (req, res, next) => {
    try {
      let getUserId = await helperController.parseTokenToGetId(
        req.headers.authorization
      );
      // console.log(req);
      let responseData = await userController.updateUser(
        req.body,
        getUserId,
        req
      );
      if (responseData !== "error") {
        responseFormat(res, 200, "success", responseData);
      } else {
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      console.log(error);
      responseFormat(res, 409, "failed", error);
    }
  }
);

router.put(
  "/updateUser/:id",
  passport.authorize("jwt"),
  upload.single("image"),
  async (req, res, next) => {
    try {
      let getUserId = req.params.id;      
      // console.log(req);
      let responseData = await userController.updateUser(
        req.body,
        getUserId,
        req
      );
      if (responseData !== "error") {
        responseFormat(res, 200, "success", responseData);
      } else {
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      console.log(error);
      responseFormat(res, 409, "failed", error);
    }
  }
);

router.put(
  "/updateUser/:user_id",
  passport.authorize("jwt"),
  async (req, res, next) => {
    try {
      let responseData = await userController.updateUser(
        req.body,
        req.params.user_id,
        req
      );
      if (responseData !== "error") {
        responseFormat(res, 200, "success", responseData);
      } else {
        responseFormat(res, 409, "failed", responseData);
      }
    } catch (error) {
      console.log(error);
      responseFormat(res, 409, "failed", error);
    }
  }
);

router.post("/getuserdata", async (req, res, next) => {
  try {
    let responseData = await userController.getuserdata(req.body);
    if (responseData !== "error") {
      responseFormat(res, 200, "success", responseData);
    } else {
      responseFormat(res, 409, "failed", responseData);
    }
  } catch (error) {
    console.log(error);
    responseFormat(res, 409, "failed", error);
  }
});

router.get("/gettrainerdata", async (req, res, next) => {
  try {
    let responseData = await userController.gettrainerdata(req.body);
    if (responseData !== "error") {
      responseFormat(res, 200, "success", responseData);
    } else {
      responseFormat(res, 409, "failed", responseData);
    }
  } catch (error) {
    console.log(error);
    responseFormat(res, 409, "failed", error);
  }
});

router.get("/unsubscribe", async (req, res, next) => {
  try {
    console.log("req.query...", req.query);
    let responseData = await userController.updateSubscription(req.query);
    if (responseData !== "error") {
      responseFormat(res, 200, "success", responseData);
    } else {
      responseFormat(res, 409, "failed", responseData);
    }
  } catch (error) {
    responseFormat(res, 409, "failed", error);
  }
});

router.get("/login-logs", passport.authorize("jwt"), async (req, res, next) => {
  try {
    var data = await userController.loginLogList(req.query);
    responseFormat(res, 200, "success", data);
  } catch (error) {
    responseFormat(res, 422, "failed", error);
  }
});

router.get("/userDashboard", passport.authorize("jwt"), async (req, res, next) => {
  try {
    var data = await userController.userDashboard(req.query);
    responseFormat(res, 200, "success", data);
  } catch (error) {
    responseFormat(res, 422, "failed", error);
  }
});

router.get("/getCountry", passport.authorize("jwt"), async (req, res, next) => {
  try {
    var data = await userController.getCountry(req.query);
    responseFormat(res, 200, "success", data);
  } catch (error) {
    responseFormat(res, 422, "failed", error);
  }
});

router.get("/getCity/:country", passport.authorize("jwt"), async (req, res, next) => {
  try {
    var data = await userController.getCity(req.params.country);
    responseFormat(res, 200, "success", data);
  } catch (error) {
    responseFormat(res, 422, "failed", error);
  }
});

router.get("/getCountryCount/:country", passport.authorize("jwt"), async (req, res, next) => {
  try {
    var data = await userController.getCountryCountDetails(req.params.country);
    responseFormat(res, 200, "success", data);
  } catch (error) {
    responseFormat(res, 422, "failed", error);
  }
});

module.exports = router;
