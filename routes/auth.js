var express = require("express");
var router = express.Router();
const authController = require("../controller/auth");
var passport = require("passport");
const { check, oneOf, validationResult } = require("express-validator");
const helperController = require("../helper/_helper");
const forgotPasswordController = require("../controller/forgotPassword");
const userProfileController = require("../controller/user_management");

router.post("/sendPinToMail", async(req, res, next) => {
    try {
        let response = await authController.sentPinToMail(req.body.email);
        if (response == "Email Send Successfully") {
            responseFormat(res, 200, "success", response);
        } else {
            responseFormat(res, 422, "failed", response);
        }
    } catch (error) {
        console.log("error" , error);
        responseFormat(res, 422, "failed", error);
    }
});

router.put("/verificationEmail", async(req, res, next) => {
    try {
        let response = await authController.verificationEmail(req.body);
        console.log(response.status == 200);
        if (response.status == 200) {
            responseFormat(res, 200, "success", response.msg);
        } else {
            throw response;
        }
    } catch (error) {
        responseFormat(res, 422, "failed", error);
    }
});

router.post(
    "/changePassword",
    passport.authorize("jwt"),
    async(req, res, next) => {
        try {
            let getUserId = await helperController.parseTokenToGetId(
                req.headers.authorization
            );
            let response = await authController.changePassword(req.body, getUserId);
            responseFormat(res, 200, "success", true);
        } catch (error) {
            console.log(error);
            responseFormat(res, 422, "failed", error);
        }
    }
);

router.get("/forgotPassword", async(req, res, next) => {
    try {
        //console.log(req.query, "FP")
        let response = await forgotPasswordController.checkUserAndSendPin(
            req.query.mail
        );
        responseFormat(res, 200, "success", true);
    } catch (error) {
        responseFormat(res, 422, "failed", error);
    }
});

router.post("/forgotPassword", async(req, res, next) => {
    try {
        //console.log("enter forgotPassword");
        let response = await forgotPasswordController.checkPINAndChangePassword(
            req.body
        );
       // console.log(response);
        if (response == true) {
            responseFormat(res, 200, "success", true);
        } else {
            responseFormat(res, 422, "failed", false);
        }
    } catch (error) {
        responseFormat(res, 422, "failed", false);
    }
});
router.post(
    "/updateprofile",
    passport.authorize("jwt"), [
        check("firstname", "firstname is required")
        .isLength({ min: 1 })
        .exists(),
        check("lastname", "lastname is required")
        .isLength({ min: 1 })
        .exists(),
        check("dob", "dob is required")
        .isLength({ min: 1 })
        .exists(),
        check("pincode", "pincode is required")
        .isLength({ min: 6 })
        .exists(),
        check("phone", "phone is required")
        .isLength({ min: 10 })
        .exists()
    ],
    async(req, res, next) => {
        try {
            let getUserId = await helperController.parseTokenToGetId(
                req.headers.authorization
            );
            var payload = req.body;
            let updateData = {
                u_first_name: payload.firstname,
                u_last_name: payload.lastname,
                u_dob: payload.dob,
                u_pincode: payload.pincode,
                u_phone: payload.phone
            };
            let response = await userProfileController.saveProfile(
                updateData,
                getUserId
            );
            responseFormat(res, 200, "Profile Updated Successfully", true);
        } catch (error) {
            responseFormat(res, 422, "Profile Updated failed", false);
        }
    }
);
module.exports = router;