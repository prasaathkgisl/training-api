const knex = require("../db/connection");
const Logger = require("../lib/logger").Logger;
const util = require("util");
const localController = require("../lib/token");
const bcrypt = require("bcryptjs");
const decode = util.promisify(localController.decodeToken);
const useragent = require("useragent");
var geoip = require("geoip-lite");
const moment = require("moment");
/**
 * parseTokenToGetId function for get the user id from the auth token
 * @param {String} payload
 */
let parseTokenToGetId = async (payload) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : parseTokenToGetId started }`
    );
    const header = payload.split(" ");
    const token = header[1];
    let decoded = await decode(token);
    return decoded.sub;
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : parseTokenToGetId failed, Error: ${error} }`
    );
    throw error;
  }
};
/**
 * validateEmail to check the given data is true or false
 * @param {*} email
 */
let validateEmail = async (email) => {
  var re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let responseValue = await re.test(String(email).toLowerCase());
  return responseValue;
};
/**
 * generateOtp funcion for generate a six digit number
 * @param {Integer} max
 */
let generateOtp = (max = 999999) => {
  Logger.info(`{ FilePath: ${__filename}  ,Function : generateOtp started }`);
  return Math.floor(Math.random() * Math.floor(max));
};
/**
 * comparePass function to check the login password correct
 * @param {String} userPassword
 * @param {String} databasePassword
 */
let comparePass = async (userPassword, databasePassword) => {
  let data = await bcrypt.compareSync(userPassword, databasePassword);
  return data;
};
let checkEmailIdExist = async (email) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : checkEmailIdExist started }`
    );
    let response = await knex("users")
      .column("u_email")
      .where({
        u_email: email,
      })
      .first();
    if (response !== undefined) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : checkEmailIdExist failed, Error: ${error} }`
    );
    throw error;
  }
};
let checkUserNameExist = async (username) => {
  try {
    Logger.info(
      `{ FilePath: ${__filename}  ,Function : checkUserNameExist started }`
    );
    let response = await knex("users")
      .column("u_username")
      .where({
        u_username: username,
      })
      .first();
    if (response !== undefined) {
      return true;
    } else {
      return false;
    }
  } catch (error) {
    Logger.error(
      `{ FilePath: ${__filename}  ,Function : checkUserNameExist failed, Error: ${error} }`
    );
    throw error;
  }
};
let useragantdata = async (req) => {
  try {
    var agent = useragent.parse(req.headers["user-agent"]);
    var brow = agent.toAgent();
    var os = agent.os.toString();
    var device = agent.device.toString();
    var hostname = req.headers.host;
    var ip = req.headers["x-forwarded-for"] || req.connection.remoteAddress;
    var geo = geoip.lookup(ip);
    var finejson = {
      agent: agent,
      brow: brow,
      os: os,
      device: device,
      hostname: hostname,
      ip: ip,
      geo: geo,
    };
    return finejson;
  } catch (error) {
    return false;
  }
};
let putlog = async (type, req, data) => {
  try {
    agentdata = await useragantdata(req);
    data.agentdata = agentdata;
    if (type == "info") {
      Logger.info(data);
    } else if (type == "error") {
      Logger.error(data);
    }
    return true;
  } catch (error) {
    return false;
  }
};
let putLoginLog = async (user_id, req) => {
  try {
    var agentdata = await useragantdata(req);
    let recordistr = await knex("app_login_users").insert({
      //record login entry
      alu_user_id: user_id,
      alu_ip_address: agentdata.ip,
      alu_os: agentdata.os,
      alu_device: agentdata.device,
      alu_browser: agentdata.brow,
    });
    return true;
  } catch (error) {
    return false;
  }
};
let getUser = async (user_email) => {
  let user = await knex("users")
    .select("*")
    .where({
      u_email: user_email,
    })
    .first();
  if (user == undefined) {
    return false;
  }
  return user;
};

function ensureAuthenticatedAsAdmin(req, res, next) {
  if (!(req.headers && req.headers.authorization)) {
    return res.status(400).json({
      status: "Please log in as admin",
    });
  }
  // decode the token
  const header = req.headers.authorization.split(" ");
  const token = header[1];
  localController.decodeToken(token, (err, payload) => {
    if (err) {
      return res.status(401).json({
        status: "Token has expired",
      });
    } else {
      if (payload.u_status === false) {
        return res.status(401).json({
          error:
            "You have been banned from Olympic cards contact support for more.",
        });
      }
      if (payload.is_admin == true) {
        return knex("users")
          .where({
            u_id: parseInt(payload.sub),
          })
          .first()
          .then((user) => {
            next();
          })
          .catch((err) => {
            res.status(500).json({
              status: "error",
            });
          });
      } else {
        return res.status(401).json({
          error: "Please, Log in as admin",
        });
      }
    }
  });
}

const validateDOB = (number) => {
  let getFormat = moment(number, 'YYYY-MM-DD')
  console.log("getFormat::", getFormat, getFormat.isValid(), getFormat.format("YYYY-MM-DD"))
  console.log('number...', number)
  const regex = /^\d{4}\-\d{2}\-\d{2}?$/; // YYYY-MM-DD
  // const regex = /^\d{2}\-\d{2}\-\d{4}?$/; // MM-DD-YYYY
  // const regex = /^\d{2}\/\d{2}\/\d{4}$/ 
  // console.log("dob:tt:", number, regex.test(number))
  let age = Math.floor(moment(new Date()).diff(moment(number, 'DD/MM/YYYY'), 'years', true));
  if (regex.test(number)) { //&& age > 18
    return true;
  } else {
    return false;
  }
};

const validatePhoneNumber = (number) => {
  // const regex = /^\d{3}\-\d{3}\-\d{4}?$/; // XXX-XXX-XXXX
  // return regex.test(number);
  var reg = /^[0-9]{1,10}$/; 
  return reg.test(number);
};

const validateEmailNew = (email) => {
  const regex =
    /^[0-9a-zA-Z]+([0-9a-zA-Z]*[-._+])*[0-9a-zA-Z]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)*([0-9a-zA-Z]*[.])[a-zA-Z]{2,6}$/;
  return regex.test(email);
};

module.exports = {
  parseTokenToGetId,
  validateEmail,
  generateOtp,
  comparePass,
  checkEmailIdExist,
  checkUserNameExist,
  useragantdata,
  putlog,
  putLoginLog,
  getUser,
  ensureAuthenticatedAsAdmin,
  validateDOB,
  validatePhoneNumber,
  validateEmailNew
};
