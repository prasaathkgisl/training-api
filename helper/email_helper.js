const mailController = require("../lib/mail");
let sendWelcomeEmail = async(sub, body, toAddr) => {
    // await mailController.sendFormAsMail(sub, body, toAddr);
    try {
        // console.log(body, "Email Body Payload")
        // console.log(sub, "Email Payload")
        await mailController.sendFormAsMail(sub, body, toAddr);
    } catch (error) {}
};
module.exports = {
    sendWelcomeEmail
};