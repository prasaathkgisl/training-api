var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var _ = require("lodash");
var passport = require("passport");
var JwtStrategy = require("passport-jwt").Strategy;
var ExtractJwt = require("passport-jwt").ExtractJwt;
const moment = require("moment");
require("dotenv").config();
require("./helper/global_functions");
const cors = require("cors");
// let cron = require('node-cron');

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var usersManagement = require("./routes/user_management");
var authManagement = require("./routes/auth");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/user", usersManagement);
app.use("/auth", authManagement);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

var opts = {};
// Setup JWT options
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey = process.env.TOKEN_SECRET;

passport.use(
  new JwtStrategy(opts, function (jwtPayload, done) {
    //If the token has expiration, raise unauthorized
    // var expirationDate = new Date(jwtPayload.exp * 1000)
    // if(expirationDate < new Date()) {
    //   console.log(expirationDate)
    //   return done(null, false);
    // }
    var user = jwtPayload;
    // console.log(user)
    // done(null, user)
    const now = moment().unix();
    // check if the token has expired
    if (now > jwtPayload.exp) done("Token has expired.");
    else done(null, user);
  })
);

module.exports = app;
