// const environment = process.env.NODE_ENV;
// const config = require("../../knexfile")[environment];

// let knex = require("knex")(config);
// // export default knex;
// module.exports = {
//     knex
// };


const environment = process.env.NODE_ENV || 'development'
const config = require('../knexfile.js')[environment];
module.exports = require('knex')(config);
