/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("country_city", function (table) {
    table.increments("c_id").primary(["c_id"]);
    table.string("c_city").nullable();
    table.string("c_city_ascii").nullable();
    table.string("c_lat").nullable();
    table.string("c_lng").nullable();
    table.string("c_country").nullable();
    table.string("c_iso2").nullable();
    table.string("c_iso3").nullable();
    table.string("c_admin_name").nullable();
    table.string("c_capital").nullable();
    table.string("c_population").nullable();
    table.string("c_cid").nullable();
    table.timestamp("l_created").defaultTo(knex.fn.now());
    table.datetime("l_modified").defaultTo(null);
    table.integer("l_delete");
    table.datetime("l_deletedt");
    table.integer("l_status").defaultTo(1);
    table.engine("InnoDB");
    table.charset("utf8");
    table.collate("utf8_bin");
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {};
