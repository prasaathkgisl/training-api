exports.up = function (knex, Promise) {
  return knex.schema.createTable("users", function (table) {
    table.increments("u_id").primary(["u_id"]);
    table.string("u_first_name").nullable();
    table.string("u_last_name");
    table.date("u_dob").nullable();
    table.string("u_phone").nullable();
    table.string("u_password", [255]);
    table.string("u_email", [100]).nullable();
    table.string("u_role", [100]).nullable();
    table.integer("u_activated").nullable();
    table.integer("u_banned").nullable();
    table.string("u_ban_reason", [255]).nullable();
    table.string("u_new_password_key", [50]).nullable();
    table.datetime("u_new_password_requested");
    table.string("u_new_email", [100]).nullable();
    table.string("u_new_email_key", [255]).nullable();
    table.string("u_last_ip", [40]).nullable();
    table.datetime("u_last_login").defaultTo(null);
    table.timestamp("u_created").defaultTo(knex.fn.now());
    table.datetime("u_modified").defaultTo(null);
    table.integer("u_delete");
    table.datetime("u_deletedt");
    table.integer("u_status").defaultTo(1);
    table.engine("InnoDB");
    table.charset("utf8");
    table.collate("utf8_bin");
  });
};

exports.down = function (knex, Promise) {};
