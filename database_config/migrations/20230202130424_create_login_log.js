/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("login_log", function (table) {
    table.increments("l_id").primary(["l_id"]);
    table.string("l_user_name").nullable();
    table.integer("l_user_id").nullable();
    table.string("l_login_status").nullable();        
    table.timestamp("l_created").defaultTo(knex.fn.now());
    table.datetime("l_modified").defaultTo(null);
    table.integer("l_delete");
    table.datetime("l_deletedt");
    table.integer("l_status").defaultTo(1);
    table.engine("InnoDB");
    table.charset("utf8");
    table.collate("utf8_bin");
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {};
